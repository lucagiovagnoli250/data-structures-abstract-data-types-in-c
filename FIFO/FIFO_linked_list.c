/*
 *  FIFO_linked_list.c
 *
 *  Created by Luca Giovagnoli on 06/12/10.
 */

#include <stdlib.h>
#include <stdio.h>
#include "FIFO_linked_list.h"

typedef struct element{
	void* data;
	struct element* next;
}ELEMENT;

struct fifo{
	struct element* head;
	struct element* tail;
	int n;
};

/*  FIFO typedefinita nell'header */
FIFO* FIFO_init(){
	FIFO* list = (FIFO*) malloc(sizeof(FIFO));
	list->head = NULL;
	list->tail = NULL;
	list->n=0;
	return list;
}

int FIFO_count(FIFO* list){
	return list->n;
}

void* FIFO_pop(FIFO* list){

	if(FIFO_count(list)==0){return NULL;}

	void* info = list->head->data;
	ELEMENT* libero = list->head;
	list->head = list->head->next;
	free(libero);
	(list->n)--;
	return info;
}

void FIFO_insert(FIFO* list,void* info){
	
	ELEMENT* nuovo = (ELEMENT*)malloc(sizeof(ELEMENT));
	nuovo->data = info;
	nuovo->next = NULL;
	
	if (FIFO_count(list)==0) {	//se la list è vuota
		list->head = nuovo;
		list->tail = list->head;
		(list->n)++;
		return;
	}
	
	list->tail->next = nuovo;
	list->tail = list->tail->next;
	(list->n)++;
}

void FIFO_free(FIFO* list,void (*free_data)(void*)){

	if(free_data==NULL) return;
	ELEMENT* temp = list->head;
	ELEMENT* old = list->head;

	while (temp!= NULL) {
		old = temp;
		temp = temp->next;
		free_data(old->data);
		free(old);
	}
	free(list);
}

void FIFO_print(FIFO* list, void (*print_data)(void*)){

	ELEMENT* temp = list->head;
	  
	if(list==NULL)return;
	if(FIFO_count(list)==0){
		fprintf(stderr,"Nothing to print.\n");
		return;
	}

	while (temp!= NULL) {
		print_data(temp->data);
		printf("->");
		temp = temp->next;
	}
	printf("NULL\n");
}



