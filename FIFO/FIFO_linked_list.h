/*
 *  FIFO_.h
 *
 *  Abstract Data Type.
 *  FIFO queue implemented with a linked list, inserting on head, extracting from tail.
 * 
 *  Created by Luca Giovagnoli on 06/12/10.
 */

typedef struct fifo FIFO;

FIFO* FIFO_init();
int FIFO_count(FIFO* list);
void* FIFO_pop(FIFO* list);
void FIFO_insert(FIFO* list,void* info);
void FIFO_free(FIFO* list,void (*free_data)(void*));
void FIFO_print(FIFO* list, void (*print_info)(void*));
