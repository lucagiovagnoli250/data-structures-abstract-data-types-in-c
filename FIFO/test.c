#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "FIFO_linked_list.h"

#define N 6
#define BUF 256

void print_data(void* str){
    printf("%s",(char*)str);
}

int main (int argc, const char * argv[]) {

    int i;
    char* data;  
    
    char matrix[N][BUF] = {{"France\0"},{"Paris\0"},{"England\0"},{"London\0"},{"Sweden\0"},{"Stockholm\0"}};

    FIFO* list = FIFO_init();

    for(i=0;i<N;i++){
      data = (char*)malloc(BUF*sizeof(char));
      strcpy(data,matrix[i]);
      FIFO_insert(list,data);
    }
    
    FIFO_print(list,print_data);
    free(FIFO_pop(list));
    FIFO_print(list,print_data);
    FIFO_free(list,free);

    return 0;
}
