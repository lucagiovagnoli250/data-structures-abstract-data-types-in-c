//to be completed


int double_hash(int k,int M,int tentativo){

	int h1 = hash_modulare(k,M);
	int h2 = hash_modulare(k,97)+1; //usare C(numero primo inferiore a M) invece che 97

	return (h1+tentativo*h2)%M;

}



void insert_open_addressing_linear_probing(HASH_TABLE* tabella,void* dati){

	int ind = hash_modulare((tabella->estrai_chiave)(dati),tabella->M);

	while (tabella->vettore[ind] != NULL) {
		ind = (ind+1)%(tabella->M);
	}
	tabella->vettore[ind] = dati;
	(tabella->N)++;
}

void insert_open_addressing_double_hashing(HASH_TABLE* tabella,void* dati){

	int i=0,k=(tabella->estrai_chiave)(dati),M=tabella->M;
	int ind = double_hash(k,M,i);

	while (tabella->vettore[ind] != NULL) {
		i++;
		ind = double_hash(k,M,i);
	}
	tabella->vettore[ind] = dati;
	(tabella->N)++;

}
