/*
 *  hash_table.c
 *  Created by Luca Giovagnoli on 25/04/14.
 */

#include "hash_table.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#define range_s 0
#define range_t 1
#define A (sqrt(5)-1)/2

#define hash_virgola_mobile(k,M) (((k-range_s)/(range_t-range_s))*M)
#define hash_modulare(k,M) (k%M)
#define hash_moltiplicativo_modulare(k,M) (k*A)%M


typedef struct node{
    const char* key;
	void* data;
	struct node* next;
}NODE;

typedef struct table {
	NODE** array;
	int N;
	int M;
	void (*print_data) (void*);
	void (*free_data) (void*);
} HASH_TABLE;

void free_node(NODE* elem, void(*free_data)(void*));
int hash_string(char* str, int M);

NODE* new_node(const char* key,void*data){

    NODE* node = (NODE*)malloc(sizeof(NODE));
    node->key = key;
    node->data = data;
    return node;
}

HASH_TABLE* hash_table_create(int M, void(*print_data)(void*), void(*free_data)(void*)) {

	HASH_TABLE* table = (HASH_TABLE*)malloc(sizeof(HASH_TABLE));

	table->array = (NODE**)calloc(M,sizeof(NODE*));
	table->M = M;
	table->N = 0;
	table->print_data = print_data;
	table->free_data = free_data;
	return table;
}

void hash_table_resize(HASH_TABLE* table){

    NODE** new_vector = (NODE**)calloc(2*table->M,sizeof(NODE*));
    int i=0;

    for(;i<table->M;i++){
        NODE* elem = table->array[i],*next;
        for(;elem!=NULL;elem=next){
            next = elem->next;
            int index = hash_string(elem->key,2*table->M);
            elem->next = new_vector[index];
            new_vector[index] = elem;
        }
    }

    free(table->array);
    table->array = new_vector;
    table->M *= 2;
}

float get_load_factor_alpha(HASH_TABLE* table){
	return ((float)table->N)/ ((float)table->M); //load factor alpha
}

void hash_table_insert(HASH_TABLE* table, const char* key, void* data){

    /* resize if load factor alpha > 0.75 */
    if(get_load_factor_alpha(table)>0.75) hash_table_resize(table);

    int index = hash_string(key,table->M);

    NODE* elem = new_node(key,data);
    elem->next = (table->array)[index];
    (table->array)[index] = elem;
	(table->N)++;

}

void* hash_table_get(HASH_TABLE* table,const char* key){
    NODE* elem =  table->array[hash_string(key,table->M)];

    for(;elem!=NULL;elem=elem->next){
        if(strcmp(elem->key,key)==0) return elem->data;
    }
    return NULL;
}

int hash_table_contains(HASH_TABLE* table,const char* key){
    if(hash_table_get(table,key)!= NULL) return TRUE;
    return FALSE;
}

int hash_table_remove(HASH_TABLE* table,const char* key){
    int index = hash_string(key,table->M);
    NODE* elem =  table->array[index],*last;

    //case first element of list
    if(elem==NULL) return FALSE;
    else if(strcmp(elem->key,key)==0) {
        table->array[index] = elem->next;
        free_node(elem, table->free_data);
        return TRUE;
    }

    for(last=elem,elem=elem->next;elem!=NULL;elem=elem->next){
        if(strcmp(elem->key,key)==0) {
            last->next = elem->next;
            free_node(elem, table->free_data);
            return TRUE;
        }
        last=elem;
    }
    return FALSE;
}


/** HASH FUNCTIONS **/

int hash_string(char* str, int M){

    int hash=0, a=127,i,l=strlen(str);

    for(i=0;i<l;i++){
        hash = (a*hash + str[i])%M;
    }

    return hash;
}


/** FREE **/
void free_node(NODE* elem, void(*free_data)(void*)){
    if(free_data==NULL) return;
    free_data(elem->data);
    free(elem);
}

void hash_table_free(HASH_TABLE* table){
    int i=0;
    for(;i<table->M;i++){
        NODE* elem = table->array[i], *next;

        while(elem!=NULL){
            next=elem->next;
            free_node(elem,table->free_data);
            elem=next;
        }
    }
    free(table->array);
    free(table);
}


/** PRINT **/
void hash_table_print(HASH_TABLE* table){
    int i;

    for(i=0;i<table->M;i++){
        printf("%d : ",i);
        NODE* elem = table->array[i];
        for(;elem!=NULL;elem=elem->next){
            table->print_data(elem->data);
            printf("->");
        }
        printf("NULL\n");
    }
    printf("\n");
}





