/**
 * Example of use of the hash_table.c ADT by Luca Giovagnoli.
 *
 **/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "hash_table.h"

#define N 20
#define BUF 256

void print_data(void* str){
    printf("%s",(char*)str);
}
void free_data(void* data){
    free ((char*) data);
}

int main (int argc, const char * argv[]) {

    int i;
    char* data;
    char matrix[N][BUF] = {{"France\0"},{"Paris\0"},{"England\0"},{"London\0"},{"Sweden\0"},{"Stockholm\0"},{"Germany\0"},
        {"Berlin\0"},{"Norway\0"}, {"Oslo\0"},{"Italy\0"}, {"Rome\0"},{"Spain\0"}, {"Madrid\0"},{"Estonia\0"}, {"Tallinn\0"},{"Netherlands\0"}, {"Amsterdam\0"},{"Ireland\0"}, {"Dublin\0"}};


    HASH_TABLE* table = hash_table_create(10,print_data,free_data);

    for(i=0;i<N;i++){
        printf("Inserting element %d:\n",i);
        data = (char*)malloc(BUF*sizeof(char));
        strcpy(data,matrix[i]);
        if(hash_table_contains(table,matrix[i])==FALSE)
              hash_table_insert(table,matrix[i],data);
        hash_table_print(table);
    }

    print_data(hash_table_get(table,"France"));
    hash_table_remove(table,"Rome");
    hash_table_print(table);
    hash_table_free(table);
    return 0;
}
