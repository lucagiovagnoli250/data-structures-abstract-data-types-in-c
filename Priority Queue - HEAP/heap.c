/*
 *  PQ.c
 *
 *  Created by Luca Giovagnoli on 17/09/11.
 */

#include <stdlib.h>
#include <stdio.h>
#include "heap.h"

#define PARENT(k) (k-1)/2
#define LEFT(k) k*2+1
#define RIGHT(k)	k*2+2

struct heap{
	int (*compare)(void*,void*);
	void** array;
	int index; //keeps track of filled elements
	int N; //max length of the array
};

void fix_up(void** array,int i,int (*compare)(void*, void*));
void fix_down(void** array,int i,int N,int (*compare)(void*, void*));

void swap(void** p1,void** p2){
	void* temp = *p1;
	*p1 = *p2;
	*p2 = temp;
}

PQ* pq_create(int N, int (*compare)(void*,void*)){

	PQ* queue = (PQ*)malloc(sizeof(PQ));
	queue->compare = compare;
	queue->array = (void**)malloc(N*sizeof(void*));
	queue->index = 0;
	queue->N = N;
	return queue;
}

void resize_heap(PQ* queue){

    int i;
    void** new_array = (void**)malloc(2*queue->N*sizeof(void*));

    for(i=0;i<queue->N;i++)
        new_array[i] = queue->array[i];

    free(queue->array);
    queue->array = new_array;
    queue->N *= 2;
}

void pq_insert(PQ* queue,void* item){

	if (queue->index >= queue->N) resize_heap(queue);

	(queue->array)[queue->index] = item;
	fix_up(queue->array, queue->index, queue->compare);
	(queue->index)++;
}

void* pq_pop(PQ* queue){

	if (queue->index == 0) {
		fprintf(stderr,"\nPop error: PQ is empty!");
		return NULL;
	}

	(queue->index)--;
	swap(&(queue->array)[0],&(queue->array)[queue->index]);
	fix_down(queue->array, 0, queue->index, queue->compare);
	return (queue->array)[queue->index];
}

int pq_count(PQ* queue){
	return queue->index;
}

void pq_print(PQ* queue,void (*print_data)(void*)){

	int i,newline=4;
	for (i=0; i<queue->index; i++) {
		print_data((queue->array)[i]);
		if(i==0)printf("\n");
		else if(i%(newline-2)==0) {
            printf("\n");
            newline*=2;
		}
	}
    printf("\n------------------------------\n");
}

void fix_up(void** array,int i, int (*compare)(void*, void*)){

	for(;i>0 && compare(array[i],array[PARENT(i)])>0; i=PARENT(i)) {
		swap(&(array[i]),&(array[PARENT(i)]));
	}
}

void fix_down(void** array,int i,int index,int (*compare)(void*, void*)){

	int a;

	for(;LEFT(i)<index ; i=a){
		a = LEFT(i);
		if (RIGHT(i)<index && compare(array[RIGHT(i)],array[LEFT(i)])>0) a = RIGHT(i);
		if (compare(array[i],array[a])>0) return;
		swap(&(array[i]), &(array[a]));
	}
}

void pq_free(PQ* queue, void free_data(void*)){
    int i;
    for(i=0;i<queue->N;i++)
        free_data(queue->array[i]);

    free(queue->array);
    free(queue);
}


void pq_fix(PQ* queue){

	int i;
	for (i=PARENT(queue->index-1); i>=0; i--) {
		fix_down(queue->array, i, queue->index, queue->compare);
	}
}

