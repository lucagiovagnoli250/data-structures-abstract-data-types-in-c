/*
 *  heap.h
 *  ADT Priority Queue implemented with a HEAP.
 *
 *  Created by Luca Giovagnoli on 17/09/11.
 *
 */

#define TRUE 1
#define FALSE 0

typedef struct heap PQ;

PQ* pq_create(int n,int (*compara)(void*,void*));
int pq_count(PQ* queue);
void pq_insert(PQ* queue,void* item);
void* pq_pop(PQ* queue);
void pq_print(PQ* queue,void (*print_data)(void*));
void pq_free(PQ* queue, void free_data(void*));
void pq_fix(PQ* queue);
