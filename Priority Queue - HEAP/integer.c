/*
 *  INTEGER.c
 *
 *  Created by Luca Giovagnoli on 06/12/10.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "integer.h"

struct struttura{
	int valore;
};

INTEGER* INTEGER_init(int numero){

	INTEGER* elem = (INTEGER*)malloc(sizeof(INTEGER));
	elem->valore = numero;
	return elem;
}

void INTEGER_print(void* item){

	INTEGER* elem = (INTEGER*)item;
	printf("%d ",elem->valore);
}

int INTEGER_compare(void* elem1,void* elem2){

	INTEGER* item1 = (INTEGER*)elem1;
	INTEGER* item2 = (INTEGER*)elem2;

	return item2->valore-item1->valore;
}

void INTEGER_free(void* item){
    free((INTEGER*) item);
}





