/*
 *  integer.h
 *  Integer Wrapper
 *
 *  Created by Luca Giovagnoli on 06/12/10.
 */

#ifndef INTEGER_WRAPPER
#define INTEGER_WRAPPER

typedef struct struttura INTEGER;

INTEGER* INTEGER_init(int numero);
void INTEGER_print(void* item);
int INTEGER_compare(void* elem1,void* elem2);
void INTEGER_free(void* item);

#endif
