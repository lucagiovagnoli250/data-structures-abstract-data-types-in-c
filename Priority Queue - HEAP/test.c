#include <stdio.h>
#include <stdlib.h>
#include "integer.h"
#include "heap.h"

#define N 16

int main (int argc, const char * argv[]) {

	PQ* pq;
	int i,arr[N] = {21,41,34,10,8,12,16,23,97,86,44,75,11,80,85,65};
	INTEGER* item;

	pq = pq_create(5,INTEGER_compare);

	for (i=0;i<N; i++) {
        item = INTEGER_init(arr[i]);
		pq_insert(pq, item);
		pq_print(pq, INTEGER_print);
	}

    pq_free(pq,INTEGER_free);

    return 0;
}


