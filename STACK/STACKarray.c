/*
 * STACKarray.c
 *
 *  Created on: Sep 7, 2013
 *      Author: luca
 */

#include "STACKarray.h"


typedef struct elemento{
	void* info;
	struct elemento * next;
}ELEMENTO;

struct lifo{
	struct elemento * testa;
	int n;
};
