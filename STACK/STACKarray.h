/*
 * STACKarray.h
 * STACK implementation
 *  Created on: Sep 7, 2013
 *      Author: luca
 */

#ifndef STACKARRAY_H_
#define STACKARRAY_H_

typedef struct lifo LIFO;

LIFO* STACKinit(int N);  	//N initial number of elements.
int STACKcount(LIFO* stack);
void* STACKpop(LIFO* stack);
void STACKpush(LIFO* stack, void* info);
void STACKfree(LIFO* stack,void (*free_info)(void* info));
void STACKstampa(LIFO* stack, void (*stampa)(void* info));

#endif /* STACKARRAY_H_ */

