/*
 *  STACKlista.c
 *  Es2(ADT-categoria1-LIFO_stack)
 *
 *  Created by Luca Giovagnoli on 22/02/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#include <stdlib.h>
#include "STACKlista.h"

typedef struct elemento{
	void* info;
	struct elemento * next;
}ELEMENTO;

struct lifo{
	struct elemento * testa;
	int n;
};

LIFO* STACKinit(){
	LIFO* stack = (LIFO*)malloc(sizeof(LIFO));
	stack->testa = NULL;
	stack->n=0;
	return stack;
}

int STACKcount(LIFO* stack){
	return stack->n;
}

void STACKpush(LIFO* stack,void* info){
	
	ELEMENTO* nuovo = (ELEMENTO*)malloc(sizeof(ELEMENTO));
	nuovo->info = info;
	nuovo->next = stack->testa;
	stack->testa = nuovo;
	(stack->n)++;
}

void* STACKpop(LIFO* stack){

	if(STACKcount(stack)==0) return NULL;

	void* info = stack->testa->info;
	ELEMENTO* vecchio = stack->testa;
	stack->testa = stack->testa->next;
	free(vecchio);
	(stack->n)--;
	return info;

}

void STACKfree(LIFO* stack,void (*free_info)(void* info)){

	while(STACKcount(stack)>0){
		void* info = STACKpop(stack);
		free_info(info);
	}
	free(stack);
}

void STACKstampa(LIFO* stack, void (*stampa)(void* info)){
	
	ELEMENTO* elem;
	
	for (elem = stack->testa; elem!=NULL; elem=elem->next) {
		stampa(elem->info);
	}

}

