/*
 *  STACKlista.h
 *  Es2(ADT-categoria1-LIFO_stack)
 *LIFO
 *  Created by Luca Giovagnoli on 22/02/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef STACKLISTA_H_
#define STACKLISTA_H_

typedef struct lifo LIFO;

LIFO* STACKinit();
int STACKcount(LIFO* stack);
void* STACKpop(LIFO* stack);
void STACKpush(LIFO* stack, void* info);
void STACKfree(LIFO* stack,void (*free_info)(void* info));
void STACKstampa(LIFO* stack, void (*stampa)(void* info));


#endif /* STACKLISTA_H_ */
